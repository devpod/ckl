# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(unique=True)),
                ('title', models.CharField(max_length=300)),
                ('content', models.TextField()),
                ('image', models.URLField(null=True, blank=True)),
                ('publishing_date', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('profile', models.URLField(null=True, blank=True)),
                ('twitter', models.CharField(max_length=50, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Site',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('slug', models.SlugField()),
                ('url', models.URLField()),
                ('xpath_articles_urls', models.CharField(max_length=300)),
                ('xpath_title', models.CharField(max_length=300)),
                ('xpath_content', models.CharField(max_length=300)),
                ('xpath_image', models.CharField(max_length=300)),
                ('xpath_publishing_date', models.CharField(max_length=300)),
                ('xpath_author_name', models.CharField(max_length=300)),
                ('xpath_author_profile', models.CharField(max_length=300)),
                ('xpath_author_twitter', models.CharField(max_length=300)),
            ],
        ),
        migrations.AddField(
            model_name='author',
            name='site',
            field=models.ForeignKey(to='news.Site'),
        ),
        migrations.AddField(
            model_name='article',
            name='author',
            field=models.ForeignKey(to='news.Author'),
        ),
        migrations.AddField(
            model_name='article',
            name='site',
            field=models.ForeignKey(to='news.Site'),
        ),
    ]
