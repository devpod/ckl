# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20150902_0007'),
    ]

    operations = [
        migrations.CreateModel(
            name='Outlet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300)),
                ('slug', models.CharField(max_length=300)),
                ('url', models.URLField(unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='site',
            name='xpath_outlet_title',
            field=models.CharField(max_length=300, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='site',
            name='xpath_outlet_url',
            field=models.CharField(max_length=300, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='site',
            name='xpath_author_profile',
            field=models.CharField(max_length=300, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='site',
            name='xpath_author_twitter',
            field=models.CharField(max_length=300, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='site',
            name='xpath_image',
            field=models.CharField(max_length=300, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='site',
            name='xpath_publishing_date',
            field=models.CharField(max_length=300, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='outlet',
            name='site',
            field=models.ForeignKey(to='news.Site'),
        ),
        migrations.AddField(
            model_name='article',
            name='outlet',
            field=models.ForeignKey(blank=True, to='news.Outlet', null=True),
        ),
    ]
