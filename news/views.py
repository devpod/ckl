from django.shortcuts import redirect
from rest_framework import viewsets
from news.models import Article, Author, Outlet
from news.serializers import ArticleSerializer, AuthorSerializer, OutletSerializer


def home(request):
    return redirect('/api/')


class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    http_method_names = ['get', 'head', 'options']


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    http_method_names = ['get', 'head', 'options']


class OutletViewSet(viewsets.ModelViewSet):
    queryset = Outlet.objects.all()
    serializer_class = OutletSerializer
    http_method_names = ['get', 'head', 'options']
