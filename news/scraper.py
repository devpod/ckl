from bs4 import BeautifulSoup
from datetime import datetime
from django.utils.text import slugify
import requests
from lxml import etree
from unidecode import unidecode
from news.models import Author, Article, Outlet


def extract(html, query):
    result = html.xpath(query)
    try:
        return unidecode(result.pop())
    except IndexError:
        return


def crawl(url, query):
    response = requests.get(url)
    if response.status_code != 200:
        return []

    html = etree.fromstring(response.text, parser=etree.HTMLParser())
    return html.xpath(query)


def scrape(site):
    crawled_urls = crawl(site.url, site.xpath_articles_urls)
    articles_urls = site.article_set.filter(url__in=crawled_urls).values_list('url', flat=True)
    crawled_urls = set(crawled_urls) - set(articles_urls)  # Avoid scraping articles that already exists in database
    for url in crawled_urls:
        print 'Scraping %s' % url
        response = requests.get(url)
        if response.status_code != 200:
            continue
        html = etree.fromstring(response.text, parser=etree.HTMLParser())

        # Scrape author
        author_name = extract(html, site.xpath_author_name)

        author_profile = None
        if site.xpath_author_profile:
            author_profile = extract(html, site.xpath_author_profile)

        author_twitter = None
        if site.xpath_author_twitter:
            author_twitter = extract(html, site.xpath_author_twitter)
        if author_twitter:
            author_twitter = author_twitter.rsplit('/', 1)[-1]

        author, created = Author.objects.get_or_create(site=site, name=author_name, profile=author_profile, defaults={
            'twitter': author_twitter,
        })

        outlet = None
        if site.xpath_outlet_title and site.xpath_outlet_url:
            outlet_title = extract(html, site.xpath_outlet_title)
            outlet_url = extract(html, site.xpath_outlet_url)
            outlet, created = Outlet.objects.get_or_create(title=outlet_title, url=outlet_url, site=site, defaults={
                'slug': slugify(outlet_title),
            })

        # Scrape article
        article = Article(site=site, url=url, author=author, outlet=outlet)
        article.title = extract(html, site.xpath_title)

        if site.xpath_image:
            article.image = extract(html, site.xpath_image)

        if site.xpath_publishing_date:
            article.publishing_date = datetime.fromtimestamp(int(extract(html, site.xpath_publishing_date)))

        content = []
        for p in html.xpath(site.xpath_content):
            # Text cleanup
            soup = BeautifulSoup(etree.tostring(p), 'lxml')

            for script in soup(['script', 'style']):
                script.extract()

            content.append(soup.get_text())

        article.content = unidecode(u' '.join(content).replace('\n', ''))
        article.save()
