from django.contrib import admin
from news.models import Site, Author, Article, Outlet


class SiteAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'url')
    prepopulated_fields = {
        'slug': ('name',)
    }

admin.site.register(Site, SiteAdmin)


class AuthorAdmin(admin.ModelAdmin):
    list_display = ('name', 'twitter', 'profile')

admin.site.register(Author, AuthorAdmin)


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'url', 'publishing_date')

admin.site.register(Article, ArticleAdmin)


class OutletAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'url')

admin.site.register(Outlet, OutletAdmin)