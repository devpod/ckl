from rest_framework import serializers
from news.models import Article, Author, Outlet


class AuthorSerializer(serializers.ModelSerializer):

    site = serializers.StringRelatedField()

    class Meta:
        model = Author
        fields = ('name', 'profile', 'twitter', 'site')


class ArticleSerializer(serializers.ModelSerializer):

    author = serializers.StringRelatedField()
    site = serializers.StringRelatedField()
    outlet = serializers.StringRelatedField()

    class Meta:
        model = Article
        fields = ('url', 'title', 'content', 'author', 'site', 'publishing_date', 'outlet')


class OutletSerializer(serializers.ModelSerializer):

    class Meta:
        model = Outlet
        fields = ('title', 'slug', 'url')