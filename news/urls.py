from django.conf.urls import include, url
from rest_framework import routers
from news import views

router = routers.DefaultRouter()
router.register(r'articles', views.ArticleViewSet)
router.register(r'authors', views.AuthorViewSet)
router.register(r'outlets', views.OutletViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]