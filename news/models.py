from django.db import models


class Site(models.Model):
    """Site model used by the scraper, the xpath fields are used to get the data"""

    name = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)
    url = models.URLField()

    # Scrapping properties
    xpath_articles_urls = models.CharField(max_length=300)

    # Article
    xpath_title = models.CharField(max_length=300)
    xpath_content = models.CharField(max_length=300)
    xpath_image = models.CharField(max_length=300, blank=True, null=True)
    xpath_publishing_date = models.CharField(max_length=300, blank=True, null=True)

    # Author
    xpath_author_name = models.CharField(max_length=300)
    xpath_author_profile = models.CharField(max_length=300, blank=True, null=True)
    xpath_author_twitter = models.CharField(max_length=300, blank=True, null=True)

    # Outlet
    xpath_outlet_title = models.CharField(max_length=300, blank=True, null=True)
    xpath_outlet_url = models.CharField(max_length=300, blank=True, null=True)

    def __unicode__(self):
        return self.name


class Author(models.Model):
    site = models.ForeignKey(Site)
    name = models.CharField(max_length=100)
    profile = models.URLField(blank=True, null=True)
    twitter = models.CharField(max_length=50, blank=True, null=True)

    def __unicode__(self):
        return self.name


class Outlet(models.Model):
    site = models.ForeignKey(Site)
    title = models.CharField(max_length=300)
    slug = models.CharField(max_length=300)
    url = models.URLField(unique=True)

    def __unicode__(self):
        return self.title


class Article(models.Model):
    site = models.ForeignKey(Site)
    author = models.ForeignKey(Author)
    outlet = models.ForeignKey(Outlet, blank=True, null=True)
    url = models.URLField(unique=True)
    title = models.CharField(max_length=300)
    content = models.TextField()
    image = models.URLField(blank=True, null=True)
    publishing_date = models.DateTimeField()

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('site', '-publishing_date')