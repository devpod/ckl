from django.core.management.base import BaseCommand
from news import scraper
from news.models import Site


class Command(BaseCommand):

    def handle(self, *args, **options):
        for site in Site.objects.all():
            scraper.scrape(site)