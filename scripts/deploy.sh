#!/bin/bash
set -e

source ~/.virtualenvs/ckl/bin/activate

pushd ~/ckl

git fetch
git rebase origin/master

pip install -r requirements.txt

python manage.py syncdb --settings=ckl.production_settings
python manage.py migrate --settings=ckl.production_settings
python manage.py collectstatic --noinput --settings=ckl.production_settings

touch confs/uwsgi.ini

curl https://intake.opbeat.com/api/v1/organizations/d96746cb003e4483bb6068fa0da0830c/apps/0b0b7d0f28/releases/ \
    -H "Authorization: Bearer b7ee769a8ee92a1c012b3ca93fc03cb5a3c3511b" \
    -d rev=`git log -n 1 --pretty=format:%H` \
    -d branch=`git rev-parse --abbrev-ref HEAD` \
    -d status=completed

popd