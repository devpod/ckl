#!/bin/bash
set -e

source ~/.virtualenvs/ckl/bin/activate

pushd ~/ckl/

python manage.py scrape --settings=ckl.production_settings

popd
