beautifulsoup4==4.4.0
Django==1.8.4
djangorestframework==3.2.3
lxml==3.4.4
opbeat==3.0.4
psycopg2==2.6.1
requests==2.7.0
Unidecode==0.4.18
