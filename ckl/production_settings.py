from settings import *

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ckl',
        'USER': 'ckl',
        'PASSWORD': 'e7mVQBvehZx9rq3M',
        'HOST': '127.0.0.1',
    }
}

ALLOWED_HOSTS = ['*']

INSTALLED_APPS += (
    'opbeat.contrib.django',
)
OPBEAT = {
    'ORGANIZATION_ID': 'd96746cb003e4483bb6068fa0da0830c',
    'APP_ID': '0b0b7d0f28',
    'SECRET_TOKEN': 'b7ee769a8ee92a1c012b3ca93fc03cb5a3c3511b',
}
MIDDLEWARE_CLASSES += (
    'opbeat.contrib.django.middleware.OpbeatAPMMiddleware',
)